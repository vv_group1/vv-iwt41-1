package singleList.edittext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import singleList.AbstractSingleLinkedListImpl;
import singleList.EmptyCollectionException;
import singleList.SingleLinkedListImpl;

public class Editor {
	private AbstractSingleLinkedListImpl<AbstractSingleLinkedListImpl<String>> editor;
	private AbstractSingleLinkedListImpl<String> lista;

	public Editor() {
		editor = new SingleLinkedListImpl<AbstractSingleLinkedListImpl<String>>();
	}

	/**
	 * 
	 * @return el tamaño de la lista
	 */
	public int size() {
		return editor.size();
	}

	/**
	 * 
	 * @param linea
	 * @return la lista correspondiente a la línea pasada como parámetro
	 * @throws Si el número de línea es incorrecta lanza IllegalArgumentException
	 */
	public AbstractSingleLinkedListImpl<String> getLinea(int linea) {
		if (linea < 0 || linea > editor.size()) {
			throw new IllegalArgumentException();
		}
		return this.editor.getAtPos(linea);
	}

	/**
	 * Leemos un fichero de entrada con líneas de texto que cargaremos en nuestra
	 * lista de listas "lineas".
	 * 
	 * @param nombre del fichero del que leemos
	 */
	public void leerFichero(String nombreFichero) {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		String regex = "\\s+|,\\s*|\\.\\s*";
		try {
			// Apertura del fichero y creacion de BufferedReader para poder
			// hacer una lectura comoda (disponer del metodo readLine()).
			archivo = new File(nombreFichero);
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			// Lectura del fichero
			String cadena;
			while ((cadena = br.readLine()) != null) {
				String[] words = cadena.split(regex);
				lista = new SingleLinkedListImpl<String>();
				for (String s : words) {
					lista.addLast(s);
				}
				editor.addLast(lista);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// En el finally cerramos el fichero, para asegurarnos
			// que se cierra tanto si todo va bien como si salta
			// una excepcion.
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @param i       línea desde
	 * @param j       línea hasta
	 * @param palabra palabra a buscar en el texto
	 * @return cuantas apariciones de palabra hay en el editor.
	 * @throws IllegalArgumentException
	 */
	public int numPalabras(int inicio, int fin, String palabra) {
		int init = inicio;
		if (init <= 0)
			throw new IllegalArgumentException("La línea de inicio no puede ser menor o igual a cero");
		if (fin > this.editor.size())
			throw new IllegalArgumentException("La línea fin no puede ser mayor que el máximo de líneas");
		int apariciones = 0;

		while (!editor.isEmpty() && init < fin) {
			this.lista = this.editor.getAtPos(init);
			int pos = 1;
			while (pos <= this.lista.size()) {
				String cadena = this.lista.getAtPos(pos);
				if (cadena.equals(palabra)) {
					apariciones++;
				}
				pos++;
			}
			init++;
		}
		return apariciones;
	}

	/**
	 * 
	 * @return la palabra de mayor longitud
	 * @throws EmptyCollectionException
	 */
	public String mayorLongitud() {
		String mayor = null;
		if (this.editor.size() > 0) {
			for (int i = 1; i <= this.editor.size(); i++) {
				this.lista = this.editor.getAtPos(i);
				for (int pos = 1; pos <= this.lista.size(); pos++) {
					String cadena = this.lista.getAtPos(pos);
					if (mayor == null) {
						mayor = cadena;
					} else if (cadena.length() > mayor.length()) {
						mayor = cadena;
					}
				}
			}
		}
		return mayor;
	}

	/**
	 * 
	 * @param palabra
	 * @param nuevaPalabra Sustituye palabra por nuevapalabra a lo largo de todo el
	 *                     texto
	 */
	public void sustituirPalabra(String palabra, String nuevaPalabra) {
		if (this.editor.size() > 0) {
			AbstractSingleLinkedListImpl<AbstractSingleLinkedListImpl<String>> nuevoEditor = new SingleLinkedListImpl<AbstractSingleLinkedListImpl<String>>();
			int i = 1;
			do {
				AbstractSingleLinkedListImpl<String> aux = new SingleLinkedListImpl<String>();
				this.lista = this.editor.getAtPos(i);
				int j = 1;
				while (j <= this.lista.size()) {
					if (this.lista.getAtPos(j).equals(palabra)) {
						aux.addLast(nuevaPalabra);
					} else {
						aux.addLast(this.lista.getAtPos(j));
					}
					j++;
				}
				nuevoEditor.addLast(aux);
				i++;
			} while (i <= this.editor.size());
			editor = nuevoEditor;
		}
	}

	public static void main(String[] args) throws EmptyCollectionException {
		Editor editor = new Editor();
		editor.leerFichero("prueba.txt");

		System.out.println(editor.numPalabras(1, editor.size(), "esto"));
		System.out.println(editor.mayorLongitud());
		editor.sustituirPalabra("esto", "---");
		for (int i = 1; i <= editor.size(); i++) {
			System.out.println(editor.getLinea(i));
		}
	}
}
