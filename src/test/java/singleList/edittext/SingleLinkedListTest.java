package singleList.edittext;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import singleList.AbstractSingleLinkedListImpl;
import singleList.EmptyCollectionException;
import singleList.SingleLinkedListImpl;

import java.util.NoSuchElementException;

public class SingleLinkedListTest {

	private AbstractSingleLinkedListImpl<Integer> lista;
	private Integer[] valuesToTestOk;
	private Integer[] valuesToTestNotOk;

	@Before
	public void setUp() throws Exception {
		lista = new SingleLinkedListImpl<Integer>();
		valuesToTestOk = new Integer[]{0,1,2500,4999,5000};
		valuesToTestNotOk = new Integer[]{-1, 5001};
	}


	//Test para isEmpty
	@Test
	public void testIsEmpty() {
		assertTrue("El método isEmpty devolverá true ya que la lista está vacía", lista.isEmpty());
		lista.addNTimes(2,3);
		assertFalse("El método isEmpty devolverá false ya que la lista tiene elementos", lista.isEmpty());
	}


	//Test para addFirst
	@Test
	public void testAddFirst() {

	    for (int value:valuesToTestNotOk) {
            lista.addNTimes(4,4);
            lista.addFirst(value);

            assertNotEquals("Si introducimos el valor " + value + " en el primer lugar de la lista, la " +
                    "primera posición no debería haberse actualizado.", (Integer)value, lista.getAtPos(1));

            lista = new SingleLinkedListImpl<Integer>();
        }

		for (int value:valuesToTestOk) {
            lista.addNTimes(4,4);
			lista.addFirst(value);

			assertEquals("Si introducimos el valor " + value + " en el primer lugar de la lista, " +
                            "deberíamos encontrar un " + value + " en la posición 1",
					(Integer)value,lista.getAtPos(1));

			lista = new SingleLinkedListImpl<Integer>();
		}

	}

    // Test para getAtPos
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionThrownInGetAtPos_emptyList() throws IllegalArgumentException {
		lista.getAtPos(1);
	}

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionThrownInGetAtPos_negative() throws IllegalArgumentException {
        lista.addAtPos(1, 1);
        lista.addAtPos(2, 2);
        lista.addAtPos(3, 3);
        lista.addAtPos(4, 4);
	    lista.getAtPos(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionThrownInGetAtPos_shortList() throws IllegalArgumentException {
        lista.addAtPos(1, 1);
        lista.addAtPos(2, 2);
        lista.addAtPos(3, 3);
        lista.addAtPos(4, 4);
        lista.getAtPos(10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionThrownInGetAtPos_pos5() throws IllegalArgumentException {
        lista.addAtPos(1, 1);
        lista.addAtPos(2, 2);
        lista.addAtPos(3, 3);
        lista.addAtPos(4, 4);
        lista.getAtPos(5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionThrownInGetAtPos_pos0() throws IllegalArgumentException {
        lista.addAtPos(1, 1);
        lista.addAtPos(2, 2);
        lista.addAtPos(3, 3);
        lista.addAtPos(4, 4);
        lista.getAtPos(0);
    }

	@Test
	public void testGetAtPosValid() {

		lista.addAtPos(1, 1);
        lista.addAtPos(2, 2);
        lista.addAtPos(3, 3);
        lista.addAtPos(4, 4);

        for (int i = 1; i <= 4; i++)
            assertEquals("La función ha de devolver un " + i + "al leer la posición " + i + " del vector",
                    (Integer)i, lista.getAtPos(i));

	}


	//Test para addAtPos
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionThrownInAddAtPos_negative() throws IllegalArgumentException {
		lista.addAtPos(2500,-1);
	}

	@Test
	public void testAddAtPosNegValue() {

		lista.addNTimes(4, 4);
        lista.addAtPos(-1, 1);
        assertNotEquals("Al introducir un valor inválido en la posición 1 este no debe ser " +
                "introducido", (Integer)(-1), lista.getAtPos(1));
	}

    @Test
    public void testAddAtPosInvalidValue() {

        lista.addNTimes(4, 4);
        lista.addAtPos(5001, 1);
        assertNotEquals("Al introducir un valor inválido en la posición 1 este no debe ser " +
                "introducido", (Integer)(-1), lista.getAtPos(1));
    }

    @Test
    public void testAddAtPosValidValue() {
        lista.addNTimes(4, 4);

        for (int i = 0; i < valuesToTestOk.length; i++) {
            Integer elem = valuesToTestOk[i];
            lista.addAtPos(elem, 1);
            assertEquals("No se encuentra el valor " + elem + " introducido en la posición 1",
                    elem, lista.getAtPos(1));
        }
    }


	//Test isSublist
	@Test
	public void testIsSubList() {

		lista.addAtPos(1,1);
		lista.addAtPos(2,2);
		lista.addAtPos(3,3);
		lista.addAtPos(4,4);
		lista.addAtPos(5,5);

		SingleLinkedListImpl<Integer> aux1 = new SingleLinkedListImpl<Integer>();
		aux1.addAtPos(1,1);

		SingleLinkedListImpl<Integer> aux2 = new SingleLinkedListImpl<Integer>();
		aux2.addAtPos(2,1);
		aux2.addAtPos(3,2);

		SingleLinkedListImpl<Integer> aux3 = new SingleLinkedListImpl<Integer>();
		aux3.addAtPos(1,1);
		aux3.addAtPos(2,2);
		aux3.addAtPos(3,3);
		aux3.addAtPos(4,4);
		aux3.addAtPos(5,5);

		SingleLinkedListImpl<Integer> aux4 = new SingleLinkedListImpl<Integer>();
		aux4.addAtPos(3,1);
		aux4.addAtPos(4,2);
		aux4.addAtPos(5,3);

		SingleLinkedListImpl<Integer> aux5 = new SingleLinkedListImpl<Integer>();
		aux5.addAtPos(6,1);

		SingleLinkedListImpl<Integer> aux6 = new SingleLinkedListImpl<Integer>();

		SingleLinkedListImpl<Integer> aux7 = new SingleLinkedListImpl<Integer>();
		aux7.addAtPos(1,1);
		aux7.addAtPos(2,2);
		aux7.addAtPos(3,3);
		aux7.addAtPos(4,4);
		aux7.addAtPos(5,5);
		aux7.addAtPos(6,6);

		assertEquals("Si buscamos la sublista [1], debería devolver 1",1,lista.isSubList(aux1));
		assertEquals("Si buscamos la sublista [2,3], debería devolver 2",2,lista.isSubList(aux2));
		assertEquals("Si buscamos la sublista [1,2,3,4,5], debería devolver 1",1,lista.isSubList(aux3));
		assertEquals("Si buscamos la sublista [3,4,5], debería devolver 3",3,lista.isSubList(aux4));
		assertEquals("Si buscamos la sublista [6], debería devolver -1",-1,lista.isSubList(aux5));
		assertEquals("Si buscamos la sublista [], debería devolver 1",1,lista.isSubList(aux6));
		assertEquals("Si buscamos la sublista [1,2,3,4,5,6], debería devolver -1",-1,lista.isSubList(aux7));

	}

	//Test reverse
	@Test
	public void testReverseOnEmptyList() {
		lista = lista.reverse();
		assertTrue("Al hacer reverse de una lista vacía esperamos que siga vacía", lista.isEmpty());
	}

	@Test
    public void testReverse() {

        lista.addAtPos(3,1);
        lista.addAtPos(2,2);
        lista.addAtPos(1,3);

        AbstractSingleLinkedListImpl<Integer> esperada = new SingleLinkedListImpl<Integer>();

        esperada.addAtPos(1,1);
        esperada.addAtPos(2,2);
        esperada.addAtPos(3,3);

        lista = lista.reverse();
        assertEquals(lista.toString(), esperada.toString());
    }

    //Test indexOff
    @Test(expected = NoSuchElementException.class)
	public void testThrownExceptionOnIndexOf_noElement() {
		lista.addAtPos(5, 1);
		lista.addAtPos(10, 2);
		lista.addAtPos(15, 3);

		lista.indexOf(20);
	}

    @Test
    public void testIndexOf() {
        lista.addAtPos(5, 1);
        lista.addAtPos(10, 2);
        lista.addAtPos(15, 3);

        assertEquals(lista.indexOf(10), 2);
    }

    //Test size
	@Test
	public void testSize() {
		assertEquals("Si una lista esta vacía, no tiene elementos dentro",lista.size(),0);

		lista.addAtPos(1,1);
		assertEquals("Si una lista tiene 1 elemento, su tamaño es 1", lista.size(),1);

		lista.addNTimes(1,500);

		assertEquals("Si una lista tiene n elementos, su tamaño es n", lista.size(),501);
	}

	//Test addLast
	@Test
	public void testAddLastValidValue() {

		for (Integer value:valuesToTestOk) {
		    int size = lista.size();
			lista.addLast(value);
			assertEquals((Integer)lista.getAtPos(size+1),value);
			assertEquals(size+1, lista.size());
		}
	}

	@Test
    public void testAddLastNegVal() {
	    lista.addLast(-1);
	    assertNotEquals((Integer)lista.getAtPos(lista.size()), (Integer)(-1));
    }

    @Test
    public void testAddLastBigVal() {
        lista.addLast(5001);
        assertNotEquals((Integer)lista.getAtPos(lista.size()), (Integer)(5001));
    }

	//Test removeLast
	@Test
	public void testRemoveLast() throws EmptyCollectionException {
		lista.addNTimes(2,3);
		lista.addNTimes(4,2);

		lista.removeLast();
		assertEquals((Integer)lista.size(),Integer.valueOf(4));

		lista.removeLast();
		assertEquals((Integer)lista.size(),Integer.valueOf(3));
	}

	@Test(expected = EmptyCollectionException.class)
	public void testExceptionThrownInRemoveLast_emptyList() throws EmptyCollectionException{
		lista.removeLast();
	}

	@Test(expected = NoSuchElementException.class)
	public void testExceptionThrownInRemoveLast_noElement() throws NoSuchElementException,EmptyCollectionException {
		lista.addNTimes(1,5);
		System.out.println(lista.removeLast(3));
	}

	@Test
	public void testRemoveLastElementN() throws NoSuchElementException,EmptyCollectionException{

		lista.addNTimes(4,3);
		lista.addLast(5);

		int sizeAnterior = lista.size();
		lista.removeLast(4);
		assertEquals("La lista deberá tener 1 valor menos si borramos el " +
                "último 4.",sizeAnterior-1,lista.size());

	}

	//Test addNTimes
    @Test
    public void testAddNTimes0Times() {
        lista.addNTimes(1, 0);
        assertTrue(lista.isEmpty());
    }

	@Test
	public void testAddNTimesValidValue(){

		for (Integer value: valuesToTestOk) {
			lista.addNTimes(value,1);

			assertEquals("Si introducimos 1 elementos en la lista, el tamaño de la misma deberá " +
                    "ser 1", 1,lista.size());
			assertEquals((Integer)value, (Integer)lista.getAtPos(1));

			lista = new SingleLinkedListImpl<Integer>();
		}

	}

	@Test
    public void testAddNTimesInvalidValue() {
	    for (Integer value: valuesToTestNotOk) {
            lista.addNTimes(value, 1);
            assertNotEquals((Integer) value, lista.getAtPos(1));
        }

    }

	@Test (expected = IllegalArgumentException.class)
    public void testAddNTimesNegTimes() throws IllegalArgumentException{
        lista.addNTimes(1, -1);
    }
}
