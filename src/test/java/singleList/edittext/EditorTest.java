package singleList.edittext;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import singleList.AbstractSingleLinkedListImpl;
import singleList.SingleLinkedListImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;

public class EditorTest {

    private Editor editor;
    private SingleLinkedListImpl<String> lista;
    private LinkedList<File> ficheros;

    @Before
    public void setUp() throws Exception {
        editor = new Editor();
        lista = new SingleLinkedListImpl<>();
        ficheros = new LinkedList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumPalabras_IllegalArgumentExceptionOnInit() {
        editor.numPalabras(-1, 2, "prueba");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumPalabras_IllegalArgumentExceptionOnFin() {
        editor.numPalabras(1, 2, "prueba");
    }

    @Test
    public void testNumPalabras_EditorVacio() {
        Assert.assertEquals("Al tener un editor vacío, la salida será 0",
                0, editor.numPalabras(1, -1, "prueba"));
    }

    @Test
    public void testNumPalabras_initMayorQueFin() {
        editor.leerFichero(createFile("hola", "testNumPalabras.txt"));
        Assert.assertEquals("Al ser init mayor que fin, la salida será 0",
                0, editor.numPalabras(2, 1, "prueba"));
    }

    @Test
    public void testNumPalabras_noPalabrasIgualesEnEditor() {
        editor.leerFichero(createFile("hola \n que tal \n ", "testNumPalabras.txt"));
        Assert.assertEquals("Al no encontrar la palabra prueba en editor, la salida será 0",
                0, editor.numPalabras(1, editor.size(), "prueba"));
    }

    @Test
    public void testNumPalabras_palabrasIgualesEnEditor() {
        editor.leerFichero(createFile("hola esto es una prueba \n si, es una prueba \n ",
                "testNumPalabras.txt"));
        Assert.assertEquals("Al encontrar 2 veces prueba en editor, la salida será 2",
                2, editor.numPalabras(1, editor.size(), "prueba"));
    }


    @Test
    public void testMayorLongitudEditorVacio() {
        Assert.assertEquals("En un editor vacio la salida debe ser null",
                null, editor.mayorLongitud());
    }

    @Test
    public void TestMayorLongitudFicheroVacio() {
        editor.leerFichero(createFile("", "testMayorLongitud.txt"));
        Assert.assertEquals("Con un fichero vacio la salida debe ser null",
                null, editor.mayorLongitud());
    }

    @Test
    public void TestMayorLongitudUnElemento() {
        editor.leerFichero(createFile("Hola", "testMayorLongitud.txt"));
        Assert.assertEquals("En un editor con solo una palabra, la salida debe ser esa palabra",
                "Hola", editor.mayorLongitud());
    }

    @Test
    public void TestMayorLongitudSegundoMayorElemento() {
        editor.leerFichero(createFile("Hola buenas", "testMayorLongitud.txt"));
        Assert.assertEquals("Si la segunda palabra mas larga que la primera, debe devolver la de mayor longitud",
                "buenas", editor.mayorLongitud());
    }

    @Test
    public void TestMayorLongitudPrimerMayorElemento() {
        editor.leerFichero(createFile("Hola Ana", "testMayorLongitud.txt"));
        Assert.assertEquals("Si la primera palabra es mas larga que la segunda debe devolver la de mayor longitud",
                "Hola", editor.mayorLongitud());
    }

    @Test
    public void testSustituirPalabraEditorVacio() {
        String palabra = "a", nueva_palabra = "b";
        editor.sustituirPalabra(palabra, nueva_palabra);
        Assert.assertEquals("Al sustituir una palabra con el editor vacio, su tamaño deberia ser 0",
                0, editor.size());
    }

    @Test
    public void testSustituirPalabraListaVacia() {
        String palabra = "a", nueva_palabra = "b";
        editor.leerFichero(createFile(" ", "testSustituirPalabras.txt"));
        editor.sustituirPalabra(palabra, nueva_palabra);
        Assert.assertEquals("Al sustituir una palabra en una lista vacia no deberia hacer ninguna insercion" ,
                0, editor.getLinea(1).size());
    }

    @Test
    public void testSustituirPalabraSustituirA() {
        String palabra = "a", nueva_palabra = "b";
        editor.leerFichero(createFile("a", "testSustituirPalabras.txt"));
        editor.sustituirPalabra(palabra, nueva_palabra);
        Assert.assertEquals("El editor deberia sustituir la palabra","b", editor.getLinea(1).getAtPos(1));
    }

    @Test
    public void testSustituirPalabraNoSustituir() {
        String palabra = "a", nueva_palabra = "b";
        editor.leerFichero(createFile("NoSoyA", "testSustituirPalabras.txt"));
        editor.sustituirPalabra(palabra, nueva_palabra);
        Assert.assertEquals("Al no encontrar la palabra no deberia sustituir","NoSoyA", editor.getLinea(1).getAtPos(1));
    }

    @Test
    public void testSustituirPalabraDosListasVacias() {
        String palabra = "a", nueva_palabra = "b";
        editor.leerFichero(createFile(" \n ", "testSustituirPalabras.txt"));
        editor.sustituirPalabra(palabra, nueva_palabra);
        Assert.assertEquals(2, editor.size());
    }

    @After
    public void deleteFile() {

        ficheros.add(new File("testMayorLongitud.txt"));
        ficheros.add(new File("testNumPalabras.txt"));
        ficheros.add(new File("testSustituirPalabras.txt"));

        for (File fichero : ficheros) {
            fichero.delete();
        }
    }

    //Crea un fichero con contenido msg y nombre name

    private String createFile(String msg, String name) {
        String filename = name;
        FileWriter fichero = null;
        PrintWriter pw;
        try {
            fichero = new FileWriter(filename);
            pw = new PrintWriter(fichero);

            pw.print(msg);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return filename;
    }
}